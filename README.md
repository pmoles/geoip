# Geo IP
Short python script to retrieve the external IP and Location of the current system.

## Compatibility
Mac OS X and Linux distributions.

## Requirements
* [emoji](https://pypi.python.org/pypi/emoji/0.3.9)
* [simplejson](https://pypi.python.org/pypi/simplejson/)
* [IPy](https://pypi.python.org/pypi/IPy/)

## Usage
```bash
$ python geoip.py
```

## License
Copyright (C) 2016, PMoles

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.