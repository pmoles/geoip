# -*- coding: UTF-8 -*-

import subprocess
import urllib2
import emoji
import simplejson as simplejson
import sys
from IPy import IP


def main():

	if len(sys.argv) >= 2 and sys.argv[1] in ['--help', '-h']:
		print 'Usage: python geoip.py [-h|--help|ipaddress] [-a|--all]'
		print 'If no parameter is provided, external IP of the system is used'
		return 0
	elif len(sys.argv) >= 2 and sys.argv[1] not in ['--all', '-a']:
		ip = sys.argv[1]
	else:
		ip = IP((subprocess.check_output(["dig", "myip.opendns.com", "@resolver1.opendns.com", "+short"])).strip())

	try:
		ipObj = IP(ip)
	except Exception:
		print 'Invalid IP, please provide a valid IP'
		return 1

	ipjson = simplejson.loads(urllib2.urlopen('http://ip-api.com/json/%s' % ipObj.strNormal()).read())
	if len(sys.argv) == 2 and sys.argv[1] in ['--all', '-a']:
		print simplejson.dumps(ipjson, indent=4, sort_keys=True)
	elif len(sys.argv) == 3 and sys.argv[2] in ['--all', '-a']:
		print simplejson.dumps(ipjson, indent=4, sort_keys=True)
	print "IP:", ipObj.strNormal()
	print "Location:", ipjson['city'], emoji.emojize(':flag_for_%s: ' % ipjson['country'])


if __name__ == '__main__':
	main()